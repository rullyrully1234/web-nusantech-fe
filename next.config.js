const withSVG = require('./webpack-extends/svgr')

module.exports = withSVG({
  swcMinify: true,
  async rewrites() {
    return [
      {
        source: '/ubox-api/:path*',
        destination: `${process.env.NEXT_PUBLIC_BACKEND_URL}/:path*`,
      },
    ]
  },
})
